import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgOnChangesComponent } from './components/ng-on-changes/ng-on-changes.component';

import { FormsModule } from '@angular/forms';
import { NgOnInitComponent } from './components/ng-on-init/ng-on-init.component';
import { CombinadoComponent } from './components/combinado/combinado.component';
import { DocheckComponent } from './components/docheck/docheck.component';
import { CicloshooksComponent } from './components/cicloshooks/cicloshooks.component';

@NgModule({
  declarations: [
    AppComponent,
    NgOnChangesComponent,
    NgOnInitComponent,
    CombinadoComponent,
    DocheckComponent,
    CicloshooksComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
