import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-ng-on-init',
  templateUrl: './ng-on-init.component.html',
  styleUrls: ['./ng-on-init.component.css']
})
export class NgOnInitComponent implements OnInit {

  @Input() public entradaInput: string;

  cadena = "";

  constructor() { }

  ngOnInit(): void {
    // console.log(this.entradaInput);

    for (let index = 0; index < this.entradaInput.length; index++) {
        this.cadena+=index;
    }

  }

}
