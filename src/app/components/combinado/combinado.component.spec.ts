import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CombinadoComponent } from './combinado.component';

describe('CombinadoComponent', () => {
  let component: CombinadoComponent;
  let fixture: ComponentFixture<CombinadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CombinadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CombinadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
