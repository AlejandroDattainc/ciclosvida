import { Component, OnInit, OnChanges, Input, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-combinado',
  templateUrl: './combinado.component.html',
  styleUrls: ['./combinado.component.css']
})
export class CombinadoComponent implements OnInit, OnChanges {

  @Input() public entradaCombinado: string;
  @Input() public nuevaEntradaCombinado: string;

  constructor() { }

  ngOnChanges( changes: SimpleChanges){

    // console.log(changes);

    if (!changes.entradaCombinado.firstChange) {
      // console.log(changes.entradaCombinado.currentValue[changes.entradaCombinado.currentValue.length-1]);
    }
    // objeto de la primera vez que se carga
    // currentValue: "Esto es un texto"
    // firstChange: true
    // previousValue: undefined

    // objeto con cambiado
    // currentValue: "Esto es "
    // firstChange: false
    // previousValue: "Esto es un texto"
    
  }

  ngOnInit(): void {
    // console.log(this.entradaCombinado[0]);
  }

}
