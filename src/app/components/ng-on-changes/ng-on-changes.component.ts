import { Component, OnChanges, Input, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-ng-on-changes',
  templateUrl: './ng-on-changes.component.html',
  styleUrls: ['./ng-on-changes.component.css']
})
export class NgOnChangesComponent implements OnChanges {

  @Input() public entrada: string;
  @Input() public nuevaEntrada: string;

  cadena = "";

  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    // console.log("Veo el cambio");
    // console.log(changes);
    this.cadena = "";
    for (let index = 0; index < this.entrada.length; index++) {
      this.cadena += index;
    }

  }

}
